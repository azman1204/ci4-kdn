<?php

// http://ci4-kdn.test/post/create
$routes->get('/post/create', 'Post::index');
$routes->get('/post/demo', 'Post::demoJquery');
$routes->post('/post/demo-data', 'Post::demoData');
$routes->get('/post/demo2', 'Post::demo2');


$routes->get('/', 'Home::index');
// http://ci4-kdn.test/news
$routes->get('/news', 'News::listing');
$routes->get('/news/create', 'News::create');
$routes->post('/news', 'News::save');
$routes->get('/edit/(:num)', 'News::edit/$1');
$routes->get('/delete/(:num)', 'News::delete/$1');

$routes->get('/home', function() {
    return view('home'); // views/home.php
});

// http://ci4-kdn.test/about
$routes->get('/about', function() {
    return view('about'); // views/home.php
});

$routes->group('/', ['filter' => 'loginfilter'], function($routes) {
    $routes->match(['GET', 'POST'], 'employee-list', 'Employee::index');
    $routes->get('employee-create', 'Employee::create');
    $routes->get('employee-edit/(:num)', 'Employee::edit/$1');
    $routes->get('employee-delete/(:num)', 'Employee::delete/$1');
    $routes->post('employee-save', 'Employee::save');
});

// session and filter demo
$routes->group('/session', ['filter' => 'demofilter'], function($routes) {
    // ci4-kdn.test/session/sess-create
    $routes->get('sess-create', 'SessionDemo::create');
    $routes->get('sess-retrieve', 'SessionDemo::retrieve');
    $routes->get('sess-delete', 'SessionDemo::delete');
    $routes->get('sess-create-flash', 'SessionDemo::createFlash');
    $routes->get('sess-show', 'SessionDemo::show');
});

// login
$routes->get('/login', 'Login::index');
$routes->post('/auth', 'Login::auth');
$routes->get('/logout', 'Login::logout');