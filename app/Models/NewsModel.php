<?php
namespace App\Models;

use CodeIgniter\Model; // import library

class NewsModel extends Model {
    protected $table = 'news'; // model ini mewakili table news
    // allow mass insert
    protected $allowedFields = ['title', 'body'];
}