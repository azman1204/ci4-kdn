<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\EmployeeModel;

class Employee extends BaseController
{
    function index() {
        helper('form'); // load helper
        // list semua data dari table employee
        // $model = model(EmployeeModel::class);
        $model = new EmployeeModel();
        //$rows = $model->findAll();
        $where = [];
        if ($this->request->getVar('name')) {
            //$where['name'] = $this->request->getVar('name');
            $model->like('name', $this->request->getVar('name'), 'both');
        }

        if ($this->request->getVar('email')) {
            $where['email'] = $this->request->getVar('email');
        }

        $rows  = $model->where($where)->paginate(2); // 1 pg. ada 2 rekod
        $pager = $model->pager;
        //$url = $pager->addQuery(['name'=>'Jane Doe']);
        //echo $url;
        //$pager = $model->pager->makeLinks(1, 2, $rows->total, 'group');
        //var_dump($rows);
        return view('employee/list', [
            'rows'  => $rows,
            'pager' => $pager
        ]);
    }

    function create() {
        $emp = [
            'name'   => '',
            'id'     => '',
            'bod'    => '',
            'salary' => '',
            'email'  => '',
            'password'  => '',
        ];
        return view('employee/form', ['emp' => $emp]);
    }

    function save() {
        $rules = [
            'name'   => 'required|min_length[5]',
            'email'  => 'required|valid_email',
            'bod'    => 'required|valid_date[Y-m-d]',
            'salary' => 'required|decimal',
            'password' => 'required|min_length[5]'
        ];
        $ok = $this->validate($rules);

        if ($ok) {
            // encrypt password
            $data = $this->request->getPost();
            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        } else {
            return view('employee/form', [
                'emp' => $this->request->getPost(), 
                'validator' => $this->validator
            ]);
        }
        
        $model = new EmployeeModel();
        $model->save($data);
        return redirect()->to('/employee-list');
    }

    function edit($id) {
        $model = new EmployeeModel();
        $emp = $model->where('id', $id)->first();
        return view('employee/form', ['emp' => $emp]);
    }

    function delete($id) {
        $model = new EmployeeModel();
        $emp = $model->delete($id);
        return redirect()->to('/employee-list');
    }
}
