<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\EmployeeModel;
use CodeIgniter\HTTP\ResponseInterface;

class Login extends BaseController
{
    // show login form
    public function index()
    {
        return view('login');
    }

    // check email and password sent against database (employee)
    function auth() {
        //var_dump($this->request->getPost());
        $model = new EmployeeModel();
        $data = $this->request->getPost();
        $emp = $model->where('email', $data['email'])->first();
        if ($emp) {
            // emp exist
            if (password_verify($data['password'], $emp['password'])) {
                //echo 'password matched';
                session()->set('logged-in', 'yes');
                session()->set('user', $emp);
                return redirect()->to("/employee-list");
            } else {
                //echo 'password not match';
                return redirect()->to("/login")
                ->with('err', 'Emel dan katalaluan tidak wujud');
            }
        } else {
            //echo 'does not exist';
            return redirect()->to("/login")
            ->with('err', 'Emel dan katalaluan tidak wujud');
        }
    }

    function logout() {
        session()->destroy();
        return redirect()->to('/login');
    }
}
