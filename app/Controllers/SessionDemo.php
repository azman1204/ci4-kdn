<?php
namespace App\Controllers;

class SessionDemo extends BaseController {
    // create session
    function create() {
        $sess = session();
        $sess->set('nama', 'John Doe');
    }

    // get / retrieve session
    function retrieve() {
        $sess = session();
        echo $sess->get('nama');
    }

    // delete session
    function delete() {
        session()->remove('nama');
        // see also ->destroy()
    }

    // flashdata session
    function createFlash() {
        $sess = session();
        //$sess->setFlashdata('username', 'abu');
        return redirect()->to('/sess-show')
                ->with('username', 'abu');
    }

    function show() {
        echo session()->getFlashdata('username');
    }
}