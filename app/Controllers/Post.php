<?php
namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PostModel;

class Post extends BaseController
{
    # insert beberapa rekod
    public function index() {
        $model = model(PostModel::class);

        if ($model->save(['content' => 'bu123@!']) === false) {
            var_dump($model->errors());
        }

        $model->save(['content' => 'Content 2', 'post_by' => 1]);
        $model->save(['content' => 'Content 3', 'post_by' => 2]);
    }

    public function demoJQuery() {
        return view('post/demo_jquery'); // app/views/post/demo_jquery.php
    }

    public function demoData() {
        $data = request()->getPost();
        $model = model(PostModel::class);
        $ok = $model->save($data);
        # ok = true, bejaya insert, false - ada validation err
        if (! $ok) {
            $errors = $model->errors();
            $err = implode($errors);
        } else {
            $err = '';
        }

        header('Content-Type: application/json');
        return json_encode([
            'ok' => $ok,
            'err' => $err
        ]);
    }

    function demo2() {
        return view('post/modal');
    }
}
