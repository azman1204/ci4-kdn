<?php
namespace App\Controllers;
use App\Models\NewsModel;

class News extends BaseController {
    function listing() {
        $rows = model(NewsModel::class)->findAll();// select * from news
        //var_dump($rows);
        return view('news/list', ['nama' => 'John Doe', 'news' => $rows]); // views/news/list.php
    }

    // show news form
    function create() {
        return view('news/form');
    }

    function save() {
        echo 'coming soon...';
        // return semua data dari form dlm bentuk Array
        $data = $this->request->getPost();
        //var_dump($data);
        $model = model(NewsModel::class);
        $model->save($data); // insert data into table
        return redirect()->to('/news');
    }

    function edit($id) {
        //echo $id;
        $model = model(NewsModel::class);
        $news = $model->where(['id'=>$id])->first();
        //var_dump($news);
        return view('news/form_edit', ['news' => $news]);
    }

    function delete($id) {
        $model = model(NewsModel::class);
        $model->delete($id);
        // lepas delete senaraikan balik semua data
        return redirect()->to('/news');
    }
}