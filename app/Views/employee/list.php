<?= $this->extend('master') ?>
<?= $this->section('content') ?>
<div class="card">
    <div class="card-body">
        <a href="<?= base_url('/employee-create') ?>" class="btn btn-primary">New Employee</a>

        <!-- search bar -->`
        <?= form_open('/employee-list') ?>
            <div class="row mt-4 bg-secondary p-2">
                <div class="col-md-1">Nama</div>
                <div class="col-md-3"><input type="text" name="name" class="form-control"></div>
                <div class="col-md-1">Emel</div>
                <div class="col-md-3"><input type="text" name="email" class="form-control"></div>
                <div class="col-md-1"><input type="submit" value="Cari" class="btn btn-primary"></div>
            </div>
        <?= form_close() ?>

        <table class="table table-row-dashed table-row-gray-300 gy-2">
            <thead>
                <tr class="fw-bold fs-6 text-gray-800">
                    <th>Bil</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>BOD</th>
                    <th>Tindakan</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                foreach($rows as $emp) : ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $emp['name'] ?></td>
                    <td><?= $emp['email'] ?></td>
                    <td><?= $emp['bod'] ?></td>
                    <td>
                        <a href="<?= base_url('/employee-edit/' . $emp['id']) ?>" class="btn btn-primary btn-sm">Edit</a>
                        <a href="<?= base_url('/employee-delete/' . $emp['id']) ?>" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?= $pager->only(['name'=>'Jane Doe'])->links() ?>
    </div>
</div>

<?= $this->endSection() ?>

