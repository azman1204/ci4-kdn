<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<?php 
if (isset($validator)) : ?>
    <div class="alert alert-danger">
        <?= $validator->listErrors() ?>
    </div><?php 
endif; ?>

<form method="post" action="<?= base_url('/employee-save') ?>">
    <input type="hidden" name="id" value="<?= $emp['id'] ?>">
    <div class="col-md-6">
        <div class="row">
            <div class="col-12">
                <label>Nama</label>
                <input type="text" value="<?= $emp['name'] ?>" 
                class="form-control" name="name">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label>Emel</label>
                <input type="text" value="<?= $emp['email'] ?>" class="form-control" name="email">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label>Tarikh Lahir</label>
                <input type="date" value="<?= $emp['bod'] ?>" class="form-control" name="bod">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label>Gaji</label>
                <input type="number" value="<?= $emp['salary'] ?>" class="form-control" name="salary">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label>Katalaluan</label>
                <input type="password" value="<?= $emp['password'] ?>" class="form-control" name="password">
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12">
                <input type="submit" class="btn btn-primary">
            </div>
        </div>
    </div>
</form>

<?= $this->endSection() ?>