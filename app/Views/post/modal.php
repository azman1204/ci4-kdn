<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_1">
    Launch demo modal
</button>

<div class="modal fade" tabindex="-1" id="kt_modal_1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Modal title</h3>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">
                <div id="msg"></div>
                <form method="post" action="#" id="myform">
                    <label>Content</label>
                    <input type="text" name="content">
                    <br>
                    <label>Post By</label>
                    <input type="text" name="post_by">
                    <br>
                    <input type="submit" value="Save">
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <script>
        $(function() {
            $('#myform').submit(function(event) {
                event.preventDefault(); // jgn submit form
                var data2 = $(this).serialize();
                console.log(data2);

                $.post('/post/demo-data', data2, function(data) {
                    // convert JSON kpd object / array
                    data = JSON.parse(data);
                    //alert(data.ok);
                    if (data.ok == false) {
                        $('#msg').html(data.err);
                    } else {
                        // redirect
                        location.href = '/post/demo';
                    }
                }); // ajax
            });
            //alert('JQuery ok');
        });
    </script>
<?= $this->endSection() ?>

