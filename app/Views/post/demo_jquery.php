<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<h1>Post Form</h1>

<form method="post" action="#" id="myform">
    <label>Content</label>
    <input type="text" name="content">
    <br>
    <label>Post By</label>
    <input type="text" name="post_by">
    <br>
    <input type="submit" value="Save">
</form>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <script>
        $(function() {
            $('#myform').submit(function(event) {
                event.preventDefault(); // jgn submit form
                var data2 = $(this).serialize();
                console.log(data2);

                $.post('/post/demo-data', data2, function(data) {
                    alert(data);
                }); // ajax
            });
            //alert('JQuery ok');
        });
    </script>
<?= $this->endSection() ?>