<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Codeigniter 4 Training</title>
</head>
<body>
    <h1>Header</h1>
    <?= $this->renderSection('content') ?>
    <h1>Footer</h1>
</body>
</html>